#ifndef SISTEMAS_H
#define SISTEMAS_H
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

class Sistemas
{
private:
    int cuerpos;
    int G;
    int centroMasa;
    vector<float> posx;
    vector<float> posy;
    vector<float> velx;
    vector<float> vely;
    vector<float> masas;
public:
    Sistemas();  //Constructor
    void posicion();  //Método que calculará las posiciones de los cuerpos
};

#endif // SISTEMAS_H
