#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include "sistemas.h"

using namespace std;

Sistemas::Sistemas()
{
    float posix, posiy, velix, veliy;
    int mass, mayor = 0;
    cout << "Ingrese la cantidad de cuerpos\n";
    cin >> cuerpos;
    //G = 6.67384*pow(10,-11);
    G = 1;
    ofstream archivo("movimiento.txt", ios :: app);
    for (int i = 0; i < cuerpos; i++)
    {
        cout << "Ingrese la masa del cuerpo " << i+1 << endl;
        cin >> mass;
        masas.push_back(mass);
        if (mass > mayor)
        {
            mayor = mass;
            centroMasa = i; //Se necesitara conocer al cuerpo mas pesado
        }
        cout << "Ingrese la posicion inicial en X del cuerpo " << i+1 << endl;
        cin >> posix;
        posx.push_back(posix);
        cout << "Ingrese la posicion inicial en Y del cuerpo " << i+1 << endl;
        cin >> posiy;
        posy.push_back(posiy);
        cout << "Ingrese la velocidad inicial en X del cuerpo " << i+1 << endl;
        cin >> velix;
        velx.push_back(velix);
        cout << "Ingrese la velocidad inicial en Y del cuerpo " << i+1 << endl;
        cin >> veliy;
        vely.push_back(veliy);
        archivo << posix << "\t" << posiy << "\t";
    }
    archivo << "\n";
    archivo.close();
}

void Sistemas::posicion()
{
    int t = 1;
    int radio;  //El tiempo permanecerá invariable
    ofstream archivo("movimiento.txt", ios :: app);  //El documento de texto se abre/crea en modo app para agregar datos
    float theta, aceleraX, aceleraY, veloX, veloY, posiX, posiY;
    for (int i = 0; i < cuerpos; i ++)  //i sera la posicion en los arreglos del cuerpo al cual se le calcule la posicion
    {
        for (int j = 0; j < cuerpos; j ++)  //j sera la posicion en los arreglos de los cuerpos con los que se haran las iteraciones
        {
            radio =  pow(posx[j]-posx[i],2)+pow(posy[j]-posy[i],2);  //Calculo el radio que hay entre los cuerpos
            theta = atanf((posy[i]-posy[j])/(posx[i]-posx[j])); //Calculo el angulo entre los dos cuerpos
            if (j != i)  //Si no estoy iterando sobre el mismo cuerpo permito guardar las aceleraciones
            {
                if (i == centroMasa)
                {
                    aceleraX += G*(masas[j]/radio*(cos(theta)));
                    aceleraY += G*(masas[j]/radio*(sin(theta)));
                }
                else
                {
                    aceleraX += -G*(masas[j]/radio*(cos(theta)));  //Como la aceleracion será atractiva la pongo con menos
                    aceleraY += -G*(masas[j]/radio*(sin(theta)));  //Se debera ir "sumando" segun la cantidad de cuerpos que haya
                }
            }
        }
        veloX = velx[i]+(aceleraX*t);  //Calculo la nueva velocidad del cuerpo
        veloY = vely[i]+(aceleraY*t);
        posiX = posx[i]+(veloX*t)+((aceleraX*pow(t,2))/2);  //Calculo la nueva posicion
        posiY = posy[i]+(veloY*t)+((aceleraY*pow(t,2))/2);
        aceleraX = 0; //Reinicio los valores de las aceleraciones para iterar sobre otros cuerpos
        aceleraY = 0;
        archivo << posiX << "\t" << posiY << "\t";  //Guardo en el archivo las nuevas posiciones separadas por un tab
        velx.push_back(veloX);  //Guardo las nuevas posiciones y velocidades al final del vector
        vely.push_back(veloY);
        posx.push_back(posiX);
        posy.push_back(posiY);
    }
    velx.erase(velx.begin(),velx.begin()+cuerpos);  //Borro los antiguos valores de posicion y velocidad desde la primera hasta
    vely.erase(vely.begin(),vely.begin()+cuerpos);  //la cantidad de cuerpos que haya para que sólo queden los valores nuevos
    posx.erase(posx.begin(),posx.begin()+cuerpos);
    posy.erase(posy.begin(),posy.begin()+cuerpos);
    archivo << "\n";  //Guardo en el txt un salto de linea siempre al final
    archivo.close();   //Cierro el txt
}
